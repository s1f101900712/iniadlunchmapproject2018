from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('restaurants/<int:restaurant_id>/', views.detail, name='detail'),
    path('restaurants/create/', views.create, name='create'),
    path('restaurants/<int:restaurant_id>/update/', views.update, name='update'),
    path('restaurants/<int:restaurant_id>/delete/', views.delete, name='delete'),
]
